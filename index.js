// console.log("ohayou");

// [section] While loop
// a while loop take in an expression/condition.
// Expressions are any units of code that can be evaluated to a value.
// if the condition evaluates to be true the statements inside the code block will be executed.
// a loop will iterate a certain number of times until an expression/condition is met.

/* 
  Syntax:
    while(expression/condition){
        statements;
        iteration;
    } 
 */
    


// let count = 5;
// while (count!==0){


//     console.log("1st"+count);
//     count--;
//     console.log(count);
        

// }

// [Section] do while loop
    /* 
        
    */
   
    
    // let number= Number(prompt("Give me a number"));
    // do{
    //     console.log("Do while :"+number);
    //     number++;
    // }
    // while(number<10);

//    [Section for loop]

//     -a for loop is more flexible than while and do-while\



    // for (i = 0; i <=20;i++){
    //     console.log("the currect value is:"+ i);
    // }


    let myString = "alex";
    console.log(myString.length);

    console.log(myString[0]);
    console.log(myString[1]);
    console.log(myString[2]);
    console.log(myString[3]);


    for (let i=0; i<myString.length;i++){
        console.log(myString[i]);
    }
    // console.log[i];
    let numA=15;
    
    for(let i=0; i<=10; i++){
        console.log(i);
    }


    // Create a string named "myName" with value of "Alex"

    letmyName = "Pneumonoultramicroscopicsilicovolcanoconiosis";

    // create a loop that will print out the letters of the name individually and print out the number 3 instead when the letter to be printed is a vowel
    
    for (let i =0;i<letmyName.length;i++){
        letmyName[i].toLowerCase()==="a"||
        letmyName[i].toLowerCase()==="e"||
        letmyName[i].toLowerCase()==="i"||
        letmyName[i].toLowerCase()==="o"||
        letmyName[i].toLowerCase()==="u"
        ?console.log(3)
        :console.log(letmyName[i]);
    }

    // [Section] Continue and break statements
    
    // the continue statements allows the code to go to the next iteration of the loob w/o finishing the execution of all statements

    // the break statement is used to terminate the current loop once a match hasbeen found;


    //  Creates a loop that if the count value is divided by 2 and the remainder is 0, it will print the number and continue to the next iteration of the loop
    // - How this For Loop works:
    //     1. The loop will start at 0 for the the value of "count".
    //     2. It will check if "count" is less than the or equal to 20.
    //     3. The "if" statement will check if the remainder of the value of "count" divided by 2 is equal to 0 (e.g 0/2).
    //     4. If the expression/condition of the "if" statement is "true" the loop will continue to the next iteration.
    //     5. If the value of count is not equal to 0, the console will print the value of "count".
    //     6. The second if statement will check if the value of "count" is greater than 10. (e.g. 0)
    //     7. If the expression/condition of the second "if" statement is false the loop will proceed to the next iteration.
    //     8. The value of "count" will be incremented by 1 (e.g. count = 1)
    //     9. Then the loop will repeat steps 2 to 8 until the expression/condition of the loop is "false" or the condition of the second "if" statement

    for (let count = 0; count<=20;count++){
        // if remainder is equal to 0
        if(count % 2 === 0){
            // tells the code to continue to the next iteration of the loop
            // this ignores all statements location after the continue statement
            continue;
            
        }else if (count>10){
            // Tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should execute so long as the value of count is less than or equal to 20
            // number values after 10 will no longer be printed.
            break;
        }
        console.log("continue and break:"+count)
    }
    

    let name = "alexandro";

    for (let i=0;i<name.length;i++)
    {
        console.log(name[i])
        if(name[i].toLowerCase==="a"){
            console.log("Continue to next iteration");
            continue;
        }
        if(name[i].toLowerCase()==="d"){
            console.log("console log before the break!");
            break;
        }
        console.log(name[i]);
    }